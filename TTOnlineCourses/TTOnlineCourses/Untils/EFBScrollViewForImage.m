//
//  EFBScrollViewForImage.m
//  muefb
//
//  Created by  on 12-5-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "EFBScrollViewForImage.h"

#define ZOOM_LEVELS 4

#define CONTENT_INSET 0.0f
static void *scrollViewForImage = &scrollViewForImage;


@implementation EFBScrollViewForImage
{
   	CGFloat zoomAmount;
    
}

@synthesize imageView;
@synthesize delegate_hidden;

-(void)dealloc{
    [self removeObserver:self forKeyPath:@"frame"]; // Maintain iOS 4.x compatability

    [imageView release];
    [super dealloc];
    
}


static inline CGFloat ZoomScaleThatFits(CGSize target, CGSize source)
{
	CGFloat w_scale = (target.width / source.width);
	CGFloat h_scale = (target.height / source.height);
    
	return ((w_scale < h_scale) ? w_scale : h_scale);
}

#pragma mark ReaderContentView instance methods
#pragma remark 设置捏合放大缩小的范围大小

- (void)updateMinimumMaximumZoom
{
	CGRect targetRect = CGRectInset(CGRectMake(self.bounds.origin.x,self.bounds.origin.y,self.bounds.size.width,self.bounds.size.height), CONTENT_INSET, CONTENT_INSET);
    
	CGFloat zoomScale = ZoomScaleThatFits(targetRect.size, imageView.bounds.size);
    
	self.minimumZoomScale = zoomScale; // Set the minimum and maximum zoom scales
    
	self.maximumZoomScale = (zoomScale * ZOOM_LEVELS); // Max number of zoom levels
    
    zoomAmount=(self.maximumZoomScale-self.minimumZoomScale)/ZOOM_LEVELS;
    
}

-(id)initWithFrame:(CGRect)frame  andImgView:(UIImageView *)imgView
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.frame=frame;
        self.delaysContentTouches = NO;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.contentMode = UIViewContentModeRedraw;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
        self.autoresizesSubviews = NO;
        self.bouncesZoom = YES;
        self.delegate=self;
        self.contentOffset = CGPointMake((0.0f - CONTENT_INSET), (0.0f - CONTENT_INSET)); // Offset
        self.contentInset = UIEdgeInsetsMake(CONTENT_INSET, CONTENT_INSET, CONTENT_INSET, CONTENT_INSET);
        
        
        UITapGestureRecognizer *doubleTapOne = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)] autorelease];
        doubleTapOne.numberOfTouchesRequired = 1;
        doubleTapOne.numberOfTapsRequired = 2;
        [self addGestureRecognizer:doubleTapOne];
        
        UITapGestureRecognizer *doubleTapTwo = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)] autorelease];
        doubleTapTwo.numberOfTouchesRequired = 2;
        doubleTapTwo.numberOfTapsRequired = 2;
        [self addGestureRecognizer:doubleTapTwo];
        
        //在此添加是因为如果放在父类里面会与子类的doubleTapOne对象同时被响应
        UITapGestureRecognizer *singleTapOne=[[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singTapTohiddenTabbar:)] autorelease];
        singleTapOne.numberOfTapsRequired=1;
        singleTapOne.numberOfTouchesRequired=1;
        [self addGestureRecognizer:singleTapOne];
        
        //设置Single tap requires when double tap to fail
        [singleTapOne requireGestureRecognizerToFail:doubleTapOne];
        
        imgView.userInteractionEnabled=YES;
        self.imageView=imgView;
        
        [self updateMinimumMaximumZoom]; // Update the minimum and maximum zoom scales
        self.zoomScale = self.minimumZoomScale; // Set zoom to fit page content
        
        [self addObserver:self forKeyPath:@"frame" options:0 context:scrollViewForImage];
        
        [self addSubview:imageView];
        
    }
    
    return self;

}


- (id)initWithFrame:(CGRect)frame  andImgNamed:(UIImage *)img
{
    UIImageView *imgView =[[UIImageView alloc]initWithImage:img];
    return [self initWithFrame:frame andImgView:imgView];
}



-(void)singTapTohiddenTabbar:(UIGestureRecognizer *)recognizer
{
    if (delegate_hidden) {
        [delegate_hidden hiddenTabbar];
    }
    
}


- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    switch (recognizer.numberOfTouchesRequired) {
        case 1:
            [self zoomIncrement];
            
            break;
        case 2:
            [self zoomDecrement];
            
            break;
            
        default:
            break;
    }
    
    
}


- (void)layoutSubviews
{
    
	[super layoutSubviews];
    
    //加此处，self.frame翻转改变
    //[self correctCloudpicturesFrame:imageView.image.size withView:imageView];
    
    [self layoutViews];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (context == scrollViewForImage) // Our context
	{
		if ((object == self) && [keyPath isEqualToString:@"frame"])
		{
			CGFloat oldMinimumZoomScale = self.minimumZoomScale;
            
			[self updateMinimumMaximumZoom]; // Update zoom scale limits
            
			if (self.zoomScale == oldMinimumZoomScale) // Old minimum
			{
				self.zoomScale = self.minimumZoomScale;
			}
			else // Check against minimum zoom scale
			{
				if (self.zoomScale < self.minimumZoomScale)
				{
					self.zoomScale = self.minimumZoomScale;
				}
				else // Check against maximum zoom scale
				{
					if (self.zoomScale > self.maximumZoomScale)
					{
						self.zoomScale = self.maximumZoomScale;
					}
				}
			}
		}
	}
}


-(void)correctCloudpicturesFrame:(CGSize)imageSize withView:(UIView *)view
{
    
    CGFloat oldMinimumZoomScale = self.minimumZoomScale;
    
    [self updateMinimumMaximumZoom]; // Update zoom scale limits
    
    if (self.zoomScale == oldMinimumZoomScale) // Old minimum
    {
        self.zoomScale = self.minimumZoomScale;
    }
    else // Check against minimum zoom scale
    {
        if (self.zoomScale < self.minimumZoomScale)
        {
            self.zoomScale = self.minimumZoomScale;
        }
        else // Check against maximum zoom scale
        {
            if (self.zoomScale > self.maximumZoomScale)
            {
                self.zoomScale = self.maximumZoomScale;
            }
        }
        
    }
    
}



-(void)layoutViews
{
    
    CGSize boundsSize = self.bounds.size;
	CGRect viewFrame = imageView.frame;
    
	if (viewFrame.size.width < boundsSize.width)
        
		viewFrame.origin.x = (((boundsSize.width - viewFrame.size.width) / 2.0f) + self.contentOffset.x);
	else
		viewFrame.origin.x = 0.0f;
    
	if (viewFrame.size.height < boundsSize.height)
        
		viewFrame.origin.y = (((boundsSize.height - viewFrame.size.height) / 2.0f) + self.contentOffset.y);
	else
		viewFrame.origin.y = 0.0f;
    
	imageView.frame = viewFrame;
    
}



//放大
- (void)zoomIncrement
{
	CGFloat zoomScale = self.zoomScale;
    
	if (zoomScale < self.maximumZoomScale)
	{
		zoomScale += zoomAmount; // += value
        
		if (zoomScale > self.maximumZoomScale)
		{
			zoomScale = self.maximumZoomScale;
		}
        
		[self setZoomScale:zoomScale animated:YES];
	}
}

//缩小
- (void)zoomDecrement
{
	CGFloat zoomScale = self.zoomScale;
    
	if (zoomScale > self.minimumZoomScale)
	{
		zoomScale -= zoomAmount; // -= value
        
		if (zoomScale < self.minimumZoomScale)
		{
			zoomScale = self.minimumZoomScale;
		}
        
		[self setZoomScale:zoomScale animated:YES];
	}
}

//还原
- (void)zoomReset
{
	if (self.zoomScale > self.minimumZoomScale)
	{
		self.zoomScale = self.minimumZoomScale;
	}
}



#pragma mark UIScrollViewDelegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
	return imageView;
}

@end

