//
//  Images.h
//  Telecom
//
//  Created by Hyacinth on 14-7-5.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#ifndef Telecom_Images_h
#define Telecom_Images_h

#pragma mark -
#pragma mark - general used images
#pragma mark -

#define kGeneralBackImage @"back"

#pragma mark -
#pragma mark FrontPageViewController
#pragma mark -

#define kFrontPageTabbarItemNormalImage @"Home"
#define kFrontPageTabbarItemSelectedImage @""

#pragma mark -
#pragma mark ActivitiesViewController
#pragma mark -

#define kActivityTabbarItemSelectedImage @""
#define kActivityTabbarItemNormalImage @"Activite"

#pragma mark -
#pragma mark PersonalCenterViewController
#pragma mark -

#define kPersonalCenterTabbarItemSelectedImage @""
#define kPersonalCenterTabbarItemNormalImage @"Center"

#pragma mark -
#pragma mark MoreViewController
#pragma mark -

#define kMoreTabbarItemSelectedImage @""
#define kMoreTabbarItemNormalImage @"More"

#endif
