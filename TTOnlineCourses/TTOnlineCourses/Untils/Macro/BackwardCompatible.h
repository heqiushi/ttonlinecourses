//
//  BackwardCompatible.h
//  Telecom
//
//  Created by Hyacinth on 14-4-23.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#ifndef chelianbang_BackwardCompatible_h
#define chelianbang_BackwardCompatible_h


#ifdef __IPHONE_7_0
#define kDarkKeyboardAppearance     UIKeyboardAppearanceDark //黑色背景键盘
#define kRoundedButton              UIButtonTypeSystem //圆角button style
#else

#define kDarkKeyboardAppearance     UIKeyboardAppearanceAlert //黑色背景键盘

#define kRoundedButton              UIButtonTypeRoundedRect //圆角button style

#endif //end of #ifdef __IPHONE_7_0

#ifdef __IPHONE_6_0

#define kTextAlignmentLeft          UITextAlignmentLeft    //text align left
#define kTextAlignmentCenter        UITextAlignmentCenter //text align center
#define kTextAlignmentRight         UITextAlignmentRight //text align right

#define kLineBreakModeWrap              NSLineBreakByWordWrapping
#define kLineBreakModeCharacterWrap     NSLineBreakByCharWrapping
#define kLineBreakModeClip              NSLineBreakByClipping
#define kLineBreakModeHeadTruncation    NSLineBreakByTruncatingHead
#define kLineBreakModeTailTruncation    NSLineBreakByTruncatingTail
#define kLineBreakModeMiddleTruncation  NSLineBreakByTruncatingMiddle

#else

#define kTextAlignmentLeft          NSTextAlignmentLeft    //text align left
#define kTextAlignmentCenter        NSTextAlignmentCenter //text align center
#define kTextAlignmentRight         NSTextAlignmentRight //text align right

#define kLineBreakModeWrap              UILineBreakModeWordWrap
#define kLineBreakModeCharacterWrap     UILineBreakModeCharacterWrap
#define kLineBreakModeClip              UILineBreakModeClip
#define kLineBreakModeHeadTruncation    UILineBreakModeHeadTruncation
#define kLineBreakModeTailTruncation    UILineBreakModeTailTruncation
#define kLineBreakModeMiddleTruncation  UILineBreakModeMiddleTruncation

#endif //end of #ifdef __IPHONE_6_0


#endif// end of #ifndef chelianbang_BackwardCompatible_h


