//
//  TCReachabilityStatus.h
//  Telecom
//
//  Created by chu_mini on 14-7-19.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCReachabilityStatus : NSObject

+ (instancetype)shareInstance;

//是否有网络连接
-(BOOL)isNetConnected;

@end
