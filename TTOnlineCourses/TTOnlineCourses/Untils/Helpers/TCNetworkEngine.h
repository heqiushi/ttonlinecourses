//
//  TCNetworkEngine.h
//  Telecom
//
//  Created by Hyacinth on 14-7-6.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#import "MKNetworkEngine.h"


@interface TCNetworkEngine : MKNetworkEngine

+ (instancetype)sharedEngine;
@end
