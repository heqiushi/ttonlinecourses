//
//  TCNetworkEngine.m
//  Telecom
//
//  Created by Hyacinth on 14-7-6.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#import "TCNetworkEngine.h"
static NSString *kHostName =  @"api.coursera.org/api/catalog.v1";

@implementation TCNetworkEngine

static TCNetworkEngine *engine = nil;

+ (instancetype)sharedEngine
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		engine = [[[self class] alloc] initWithHostName:kHostName];
	});
	return engine;
}

@end
