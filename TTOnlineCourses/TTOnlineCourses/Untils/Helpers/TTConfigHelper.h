//
//  TCConfigHelper.h
//  Telecom
//
//  Created by Hyacinth on 14-7-6.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTConfigHelper : NSObject

+ (NSDictionary *)tabItemSelectedAttributes;

+ (NSDictionary *)tabItemNormalAttributes;


@end
