//
//  TCConfigHelper.m
//  Telecom
//
//  Created by Hyacinth on 14-7-6.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#import "TTConfigHelper.h"

@implementation TTConfigHelper

	//以下两个方法需要扩充
+ (NSDictionary *)tabItemSelectedAttributes
{
	return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont systemFontOfSize:13.f],[UIColor whiteColor], nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName, NSForegroundColorAttributeName,nil]];
}

+ (NSDictionary *)tabItemNormalAttributes
{
	return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIColor whiteColor],nil] forKeys:[NSArray arrayWithObjects:NSForegroundColorAttributeName,nil]];
}
@end
