//
//  TCReachabilityStatus.m
//  Telecom
//
//  Created by chu_mini on 14-7-19.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#import "TCReachabilityStatus.h"
#import "Reachability.h"

@interface TCReachabilityStatus ()
@property (nonatomic, strong) Reachability *reachAbility;
@property (nonatomic, assign) BOOL isNetConnected;

@end

@implementation TCReachabilityStatus

+ (instancetype)shareInstance
{
    static TCReachabilityStatus *serverStatus=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        serverStatus=[[TCReachabilityStatus alloc]init];
    });
    return serverStatus;
}


- (instancetype)init
{
    self=[super init];
    if (self) {
//        _reachAbility =[Reachability reachabilityForInternetConnection];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
//        [_reachAbility startNotifier];
        //设置初始值
//        [self getCurrentReachabilityStatus:_reachAbility];
    }
    return self;
}



//- (void)dealloc{
//    [_reachAbility stopNotifier];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
//}


#pragma mark - kReachabilityChangedNotification
-(void)reachabilityChanged:(NSNotification *)notification{
    Reachability *curReach = [notification object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self getCurrentReachabilityStatus:curReach];
}


- (void)getCurrentReachabilityStatus:(Reachability *)curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    switch (netStatus) {
        case NotReachable:
        {
            self.isNetConnected = NO;
        }
            break;
        case ReachableViaWiFi:{
            self.isNetConnected = YES;
        }
            break;
        case ReachableViaWWAN:{
            self.isNetConnected = YES;
        }
            break;
        default:
            break;
    }
}


-(BOOL)isNetConnected
{
    self.reachAbility =[Reachability reachabilityForInternetConnection];
    [self getCurrentReachabilityStatus:_reachAbility];
    
    return _isNetConnected;
}

@end
