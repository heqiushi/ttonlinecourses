//
//  UIImageView+TintColor.h
//  SXBuy
//
//  Created by chuyanling on 15/4/16.
//  Copyright (c) 2015年 owner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (TintColor)

- (UIImage *)newImageWithTintColor:(UIColor *)color;

@end
