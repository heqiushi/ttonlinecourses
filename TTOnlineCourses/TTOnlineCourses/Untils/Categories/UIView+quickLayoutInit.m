//
//  UIView+quickLayoutInit.m
//  TravelGuideMdd
//
//  Created by 陈曦 on 14/11/21.
//  Copyright (c) 2014年 mafengwo.com. All rights reserved.
//

#import "UIView+quickLayoutInit.h"


@implementation UIView (quickLayoutInit)

- (UILabel*)addLabelWithFontSize:(NSInteger)fontSize
                           color:(UIColor*)color
                           lines:(NSInteger)lines
                            text:(NSString *)text
                      constraint:(void(^)(MASConstraintMaker *make))block
{
    UILabel *lbl = [[UILabel alloc] init];
    [self addSubview:lbl];
    lbl.font = [UIFont systemFontOfSize:fontSize];
    if (color)
    {
        lbl.textColor = color;
    }
    if (block)
    {
        [lbl mas_makeConstraints:block];
    }
    lbl.numberOfLines = lines;
    if (text.length)
    {
        lbl.text = text;
    }
    return lbl;
}

- (UIImageView*)addImageViewWithConstraint:(void(^)(MASConstraintMaker *make))block
{
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [self addSubview:imageView];
    
    if (block)
    {
        [imageView mas_makeConstraints:block];
    }
    return imageView;
}

- (UIImageView*)addImageViewWithImage:(UIImage*)image
                           constraint:(void(^)(MASConstraintMaker *make))block
{
    UIImageView *imageView = [self addImageViewWithConstraint:block];
    if (image)
    {
        imageView.image = image;
    }
    return imageView;
}

- (UIImageView*)addImageViewWithImageName:(NSString *)imageName
                               constraint:(void (^)(MASConstraintMaker *))block
{
    UIImageView *imageView = [self addImageViewWithConstraint:block];
    if (imageName.length)
    {
        imageView.image = [UIImage imageNamed:imageName];
    }
    return imageView;
}


- (UIControl*)addControlWithConstraint:(void(^)(MASConstraintMaker *make))block
{
    UIControl *ctrl = [[UIControl alloc] init];
    [self addSubview:ctrl];
    if (block)
    {
        [ctrl mas_makeConstraints:block];
    }
    return ctrl;
}


- (UIButton*)addButtonWithTitle:(NSString*)title
                          color:(UIColor *)color
                     constraint:(void(^)(MASConstraintMaker *make))block
{
    UIButton *btn = [[UIButton alloc] init];
    if (title)
    {
        [btn setTitle:title forState:0];
    }
    if (color)
    {
        [btn setTitleColor:color forState:0];
    }
    [self addSubview:btn];
    if (block)
    {
        [btn mas_makeConstraints:block];
    }
    return btn;
}

- (UIButton*)addButtonWithConstraint:(void(^)(MASConstraintMaker *make))block
{
    UIButton *btn = [[UIButton alloc] init];
    [self addSubview:btn];
    [btn mas_makeConstraints:block];
    return btn;
}

- (UIButton*)addButtonWithImage:(UIImage*)image
                     constraint:(void(^)(MASConstraintMaker *make))block
{
    UIButton *btn = [[UIButton alloc] init];
    [btn setImage:image forState:0];
    [self addSubview:btn];
    if (block)
    {
        [btn mas_makeConstraints:block];
    }
    return btn;
}

- (UIButton*)addButtonWithUrl:(NSString*)url
                   constraint:(void(^)(MASConstraintMaker *make))block
{
    UIButton *btn = [[UIButton alloc] init];
  [btn setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]] forState:0];
    [self addSubview:btn];
    if (block)
    {
        [btn mas_makeConstraints:block];
    }
    return btn;
}

- (UIButton *)addButtonWithTitle:(NSString *)title fontSize:(CGFloat)fontSize color:(UIColor *)color image:(UIImage *)image imageSize:(CGSize)imageSize horizontalGapBetweenImageAndTitle:(CGFloat)hGap isImageLeft:(BOOL)isImageLeft contentInsets:(UIEdgeInsets)contentInsets constraint:(void (^)(MASConstraintMaker *make))block
{
    UIButton *btn = [[UIButton alloc] init];
    [self addSubview:btn];
    [btn setImage:image forState:0];
    [btn setTitle:title forState:0];
    [btn setTitleColor:color forState:0];
    btn.adjustsImageWhenHighlighted = NO;
    
    if (fontSize != 0) {
        btn.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    }
    
    [btn setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{NSFontAttributeName:btn.titleLabel.font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    CGRect labelTitleRect = [title boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    
    CGFloat width = contentInsets.left + imageSize.width + hGap + CGRectGetWidth(labelTitleRect) + contentInsets.right;
    CGFloat height = MAX(imageSize.height, CGRectGetHeight(labelTitleRect)) + contentInsets.top + contentInsets.bottom;
    
    if (isImageLeft) {
        
        btn.imageEdgeInsets = UIEdgeInsetsMake(contentInsets.top, contentInsets.left, contentInsets.bottom, contentInsets.right + hGap);
        btn.titleEdgeInsets = UIEdgeInsetsMake(contentInsets.top, contentInsets.left + hGap, contentInsets.bottom, contentInsets.right);
    } else {
        
        btn.imageEdgeInsets = UIEdgeInsetsMake(contentInsets.top, width - imageSize.width -  contentInsets.right, contentInsets.bottom, contentInsets.right);
        btn.titleEdgeInsets = UIEdgeInsetsMake(contentInsets.top, contentInsets.left - hGap, contentInsets.bottom, contentInsets.right + hGap + imageSize.width);
    }
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
    }];
    [btn mas_updateConstraints:block];
    
    return btn;
}




- (UIView*)addViewWithColor:(UIColor*)color
                 constraint:(void(^)(MASConstraintMaker *make))block
{
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = color;
    [self addSubview:v];
    if (block)
    {
        [v mas_makeConstraints:block];
    }
    return v;
}

- (id)addSubviewClass:(Class)classs
           constraint:(void (^)(MASConstraintMaker *))block
{
    if (![classs isSubclassOfClass:[UIView class]]
        && classs != [UIView class])
    {
        return nil;
    }
    UIView *view = [[classs alloc] init];
    [self addSubview:view];
    if (block)
    {
        [view mas_makeConstraints:block];
    }
    return view;
}

@end
