//
//  UIView+setFrame.h
//  TravelGuideMdd
//
//  Created by 陈曦 on 13-6-15.
//  Copyright (c) 2013年 mafengwo.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView(setFrame)

@property (nonatomic, assign) CGFloat frameX;
@property (nonatomic, assign) CGFloat frameY;
@property (nonatomic, assign) CGPoint frameOrigin;
@property (nonatomic, assign) CGFloat frameWidth;
@property (nonatomic, assign) CGFloat frameHeight;
@property (nonatomic, assign) CGSize frameSize;

@property (nonatomic, assign) CGFloat frameXAndWidth;
@property (nonatomic, assign) CGFloat frameYAndHeight;

@property (nonatomic, readonly) CGPoint boundsCenter;
@property (nonatomic, readonly) CGFloat boundsCenterX;
@property (nonatomic, readonly) CGFloat boundsCenterY;
@property (nonatomic, readonly) CGFloat boundsX;
@property (nonatomic, readonly) CGFloat boundsY;
@property (nonatomic, readonly) CGFloat boundsWidth;
@property (nonatomic, readonly) CGFloat boundsHeight;
@property (nonatomic, assign) CGSize boundsSize;

@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;

@property (nonatomic, assign) CGPoint leftTop;
@property (nonatomic, assign) CGPoint leftCenter;
@property (nonatomic, assign) CGPoint leftBottom;
@property (nonatomic, assign) CGPoint topCenter;
@property (nonatomic, assign) CGPoint bottomCenter;
@property (nonatomic, assign) CGPoint rightTop;
@property (nonatomic, assign) CGPoint rightCenter;
@property (nonatomic, assign) CGPoint rightBottom;

@end
