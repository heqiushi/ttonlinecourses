//
//  UIView+ShowTip.h
//  TravelGuideMdd
//
//  Created by 陈曦 on 13-9-13.
//  Copyright (c) 2013年 mafengwo.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView(ShowTip)

- (void)showTip:(NSString *)aString;
- (void)showTip:(NSString *)aString
           size:(CGSize)aSize
       trueSize:(BOOL)trueSize
           time:(CFTimeInterval)aTime
       fontSize:(CGFloat)aFontSize
         center:(CGPoint)aCenter;
@end
