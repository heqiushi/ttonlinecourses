//
//  UIViewController+MBProgressHud.m
//  Telecom
//
//  Created by shengli on 7/9/14.
//  Copyright (c) 2014 Hyacinth. All rights reserved.
//

#import "UIViewController+MBProgressHud.h"
#import <Objc/runtime.h>

NSString *const kHudKey = @"MBProgressHUD";

@implementation UIViewController (MBProgressHud)
//无蒙版，无文字
- (void)showLoading
{
	return [self showLoadingWithHint:nil];
}

//有蒙版，无文字
- (void)showLoadingWithMask
{
	return [self showLoadingWithMask:NO hint:nil];
}

//无蒙版，有文字
- (void)showLoadingWithHint:(NSString *)hint
{
	return [self showLoadingWithMask:NO hint:hint];
}

//有蒙版，有文字
- (void)showLoadingWithMaskAndHint:(NSString *)hint
{
	return [self showLoadingWithMask:YES hint:hint];
}

- (void)showLoadingWithMask:(BOOL)hasMask hint:(NSString *)hint
{
	self.hud = [[MBProgressHUD alloc] initWithView:self.view];
	self.view.userInteractionEnabled = NO;
	[self.view addSubview:self.hud];
	self.hud.delegate = self;
	if (hasMask) {
		self.hud.dimBackground = YES;
	}
	if (hint != nil) {
		self.hud.labelText = hint;
	}
	[self.hud show:YES];
}

- (void)hideLoading
{
	self.view.userInteractionEnabled = YES;
	[self.hud hide:YES];
}


- (void)showMessage:(NSString *)message duration:(NSTimeInterval)interval{
    
	self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.hud.mode = MBProgressHUDModeText;
    self.hud.labelText = message;
    self.hud.margin = 10.f;
    self.hud.yOffset=0;

    self.hud.removeFromSuperViewOnHide = YES;
    
    [self.hud hide:YES afterDelay:interval];
}


- (void)showMessage:(NSString *)message duration:(NSTimeInterval)interval position:(MBProgressHUD_MESSAGE_POSTION)position{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    hud.mode = MBProgressHUDModeText;
    hud.labelText = message;
    hud.labelFont = [UIFont systemFontOfSize:12];
    hud.margin = 10.f;
    
    switch (position) {
        case MBProgressHUD_MESSAGE_POSTION_CENTER:{
            hud.yOffset = 0;
        }
            break;
        case MBProgressHUD_MESSAGE_POSTION_TOP:{
            hud.yOffset = 110 - CGRectGetHeight(self.view.frame)/2;
        }
            break;
        case MBProgressHUD_MESSAGE_POSTION_BOTTOM:{
            hud.yOffset = CGRectGetHeight(self.view.frame)/2 - 40;
        }
            break;
            
        default:
            break;
    }
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hide:YES afterDelay:2.0];
}


#pragma mark -
#pragma mark MBProgressHud Delegate
#pragma mark -

- (void)hudWasHidden:(MBProgressHUD *)hud
{
	[hud removeFromSuperview];
	hud = nil;
}

#pragma mark - implement Setting and getting access method

- (void)setHud:(MBProgressHUD *)hud
{
	objc_setAssociatedObject(self, &kHudKey, hud, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (MBProgressHUD *)hud
{
	MBProgressHUD *hud =  objc_getAssociatedObject(self, &kHudKey);
	return hud;
}

@end
