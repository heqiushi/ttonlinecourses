//
//  UIViewController+MBProgressHud.h
//  Telecom
//
//  Created by shengli on 7/9/14.
//  Copyright (c) 2014 Hyacinth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"


typedef NS_ENUM (NSUInteger, MBProgressHUD_MESSAGE_POSTION)
{
	MBProgressHUD_MESSAGE_POSTION_TOP,
	MBProgressHUD_MESSAGE_POSTION_CENTER,
	MBProgressHUD_MESSAGE_POSTION_BOTTOM,
};


@interface UIViewController (MBProgressHud)<MBProgressHUDDelegate>
@property (strong, nonatomic) MBProgressHUD *hud;

- (void)showLoading;//无蒙版，无文字

- (void)showLoadingWithMask;//有蒙版，无文字

- (void)showLoadingWithHint:(NSString *)hint;//无蒙版，有文字

- (void)showLoadingWithMaskAndHint:(NSString *)hint;//有蒙版，有文字

- (void)showLoadingWithMask:(BOOL)hasMask hint:(NSString *)hint;

- (void)hideLoading;

-(void)showMessage:(NSString *)message duration:(NSTimeInterval)interval;

- (void)showMessage:(NSString *)message duration:(NSTimeInterval)interval position:(MBProgressHUD_MESSAGE_POSTION)position;

@end
