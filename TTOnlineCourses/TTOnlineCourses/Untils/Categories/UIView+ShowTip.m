//
//  UIView+ShowTip.m
//  TravelGuideMdd
//
//  Created by 陈曦 on 13-9-13.
//  Copyright (c) 2013年 mafengwo.com. All rights reserved.
//

#import "UIView+ShowTip.h"

@implementation UIView(ShowTip)

- (void)showTip:(NSString *)aString
           size:(CGSize)aSize
       trueSize:(BOOL)trueSize
           time:(CFTimeInterval)aTime
       fontSize:(CGFloat)aFontSize
         center:(CGPoint)aCenter
{
    UILabel *lable = [[UILabel alloc] initWithFrame:CGRectZero];
    lable.font = [UIFont systemFontOfSize:aFontSize];
    lable.text = aString;
    lable.tag = [@"TipsLabel" hash];
    lable.textAlignment = NSTextAlignmentCenter;
    lable.textColor = [UIColor whiteColor];
    lable.layer.cornerRadius = 8;
    lable.clipsToBounds = YES;
    
    if (trueSize)
    {
        lable.frame = CGRectMake(0,0,aSize.width,aSize.height);
    }
    else
    {
        CGSize size = [lable sizeThatFits:CGSizeMake(300, 99999)];
        size.height += aSize.height;
        size.width += aSize.width;
        CGRect rect = CGRectMake((self.bounds.size.width - size.width) / 2,
                                 (self.bounds.size.height - size.height) / 2,
                                 size.width,
                                 size.height);
        lable.frame = rect;
    }
    lable.center = aCenter;
    lable.backgroundColor = C_RGB_A(0x000000, 0.7);
    [self addSubview:lable];

    __weak UILabel *lbl = lable;
    [UIView animateWithDuration:0.5
                          delay:aTime
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         lbl.alpha = 0;
                     }
                      completion:^(BOOL finished) {
                          [lbl removeFromSuperview];
                      }];
}

- (void)showTip:(NSString *)aString
{
    [self showTip:aString size:CGSizeMake(8, 8) trueSize:NO time:1.5 fontSize:16 center:self.boundsCenter];
}

@end
