//
//  UIImageView+TintColor.m
//  SXBuy
//
//  Created by chuyanling on 15/4/16.
//  Copyright (c) 2015年 owner. All rights reserved.
//

#import "UIImage+TintColor.h"

@implementation UIImage (TintColor)

- (UIImage *)newImageWithTintColor:(UIColor *)color
{
    UIImage *newImage = nil;
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 1.0);
    [color setFill];
    CGRect rect = CGRectMake(0,0,self.size.width, self.size.height);
    UIRectFill(rect);
    
    [self drawInRect:rect
           blendMode:kCGBlendModeDestinationIn
               alpha:1.0];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
