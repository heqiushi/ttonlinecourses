//
//  UIView+quickLayoutInit.h
//  TravelGuideMdd
//
//  Created by 陈曦 on 14/11/21.
//  Copyright (c) 2014年 mafengwo.com. All rights reserved.
//

#import "TTBaseView.h"
 


// 弱引用
#define WEAKREF(ttttt) __weak __typeof__ (ttttt) w##ttttt = ttttt;
#define STRONGREF(ttttt) __strong __typeof__ (ttttt) s##ttttt = ttttt;

@interface UIView (quickLayoutInit)

// label
- (UILabel*)addLabelWithFontSize:(NSInteger)fontSize
                           color:(UIColor*)color
                           lines:(NSInteger)lines
                            text:(NSString*)text
                      constraint:(void(^)(MASConstraintMaker *make))block;
// image
- (UIImageView*)addImageViewWithImage:(UIImage*)image
                           constraint:(void(^)(MASConstraintMaker *make))block;
- (UIImageView*)addImageViewWithImageName:(NSString*)imageName
                               constraint:(void(^)(MASConstraintMaker *make))block;
- (UIImageView*)addImageViewWithImageURL:(NSString*)aUrl
                              constraint:(void(^)(MASConstraintMaker *make))block;
- (UIImageView*)addImageViewWithGifWithImageURL:(NSString*)aUrl
                                     constraint:(void(^)(MASConstraintMaker *make))block;
- (UIImageView*)addImageViewWithConstraint:(void(^)(MASConstraintMaker *make))block;

// control
- (UIControl*)addControlWithConstraint:(void(^)(MASConstraintMaker *make))block;

// button
- (UIButton*)addButtonWithTitle:(NSString*)title
                          color:(UIColor*)color
                     constraint:(void(^)(MASConstraintMaker *make))block;

- (UIButton*)addButtonWithImage:(UIImage*)image
                     constraint:(void(^)(MASConstraintMaker *make))block;

- (UIButton*)addButtonWithUrl:(NSString*)url
                   constraint:(void(^)(MASConstraintMaker *make))block;

- (UIButton*)addButtonWithConstraint:(void(^)(MASConstraintMaker *make))block;
- (UIButton *)addButtonWithTitle:(NSString *)title color:(UIColor *)color iamge:(UIImage *)image imageSize:(CGSize)imageSize horizontalGapBetweenImageAndTitle:(CGFloat)hGap isImageLeft:(BOOL)isImageLeft constraint:(void (^)(MASConstraintMaker *make))block;

// view
- (UIView*)addViewWithColor:(UIColor*)color
                 constraint:(void(^)(MASConstraintMaker *make))block;

// 通用
- (id)addSubviewClass:(Class)classs
           constraint:(void(^)(MASConstraintMaker *make))block;

@end
