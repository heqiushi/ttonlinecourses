//
//  EFBScrollViewForImage.h
//  muefb
//
//  Created by  on 12-5-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  EFBScrollViewForImageDelegate

-(void)hiddenTabbar;

@end

@interface EFBScrollViewForImage : UIScrollView <UIScrollViewDelegate>


@property (nonatomic,retain) UIImageView  *imageView;
@property (nonatomic,assign) id<EFBScrollViewForImageDelegate> delegate_hidden;


-(id)initWithFrame:(CGRect)frame  andImgNamed:(UIImage *)img;

-(id)initWithFrame:(CGRect)frame  andImgView:(UIImageView *)imgView;

-(void)zoomReset;

-(void)zoomDecrement;

-(void)zoomIncrement;

@end


