//
//  TTCourseraViewModel.h
//  TTOnlineCourses
//
//  Created by viviya on 15/11/4.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTBaseViewModel.h"


typedef NS_ENUM(NSUInteger, TT_COURSERAVIEW_MODEL_REQUEST_RESULT)
{
  TT_COURSERAVIEW_MODEL_REQUEST_RESULT_SUCCESS = 10,
  TT_COURSERAVIEW_MODEL_REQUEST_RESULT_FAILED  = 11
};


@interface TTCourseraViewModel : TTBaseViewModel


- (void)fetchCourseraInfo;//获取课堂信息

@end
