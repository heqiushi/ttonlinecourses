//
//  KLUserProfileCollectionViewCell.m
//  Kunlun
//
//  Created by chuyanling on 15/7/4.
//  Copyright © 2015年 hui.wang. All rights reserved.
//

#import "TTCourseraCell.h"

#import "UIView+quickLayoutInit.h"
#import "UIColor+QuickInit.h"
@interface TTCourseraCell ()

@property (nonatomic,strong)UIImageView *iconImageView;//头像
@property (nonatomic,strong)UILabel     *nameLable;//名称
@property (nonatomic,strong)UILabel     *workLoadLable;//工作量


@end

@implementation TTCourseraCell

+ (CGFloat)cellHeight
{
  return 70;
}


+ (NSString *)defaultIdentifier
{
  return NSStringFromClass([self class]);
}


- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    __weak UIView *mainView = self;
    
    self.backgroundColor = [UIColor colorWithRGBHexString:@"#3DB1A7"];
    
     _iconImageView = [self addImageViewWithImageName:@"kunlun_default_profile" constraint:^(MASConstraintMaker *make) {
      make.top.mas_equalTo(mainView.mas_top).offset(10);
      make.left.mas_equalTo(mainView.mas_left).offset(10);
      make.width.mas_equalTo(50);
      make.height.mas_equalTo(50);
    }];
    [self addSubview:_iconImageView];
    
    UILabel *nameTitleLable = [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"" constraint:^(MASConstraintMaker *make) {
      make.top.mas_equalTo(mainView.mas_top).offset(20);
      make.left.mas_equalTo(mainView.mas_left).offset(70);
      make.width.mas_equalTo(50);
      make.height.mas_equalTo(30);
    }];
    nameTitleLable.text = @"课程:";
    [self addSubview:nameTitleLable];
    
    _nameLable = [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"" constraint:^(MASConstraintMaker *make) {
      make.top.mas_equalTo(mainView.mas_top).offset(20);
      make.left.mas_equalTo(mainView.left).offset(120);
      make.width.mas_equalTo(130);
      make.height.mas_equalTo(30);
    }];
    [self addSubview:_nameLable];
    
    UILabel *workTitleLoadLable = [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"工作量:" constraint:^(MASConstraintMaker *make) {
      make.top.mas_equalTo(mainView.mas_top).offset(20);
      make.left.mas_equalTo(mainView.left).offset(250);
      make.width.mas_equalTo(60);
      make.height.mas_equalTo(30);
    }];
    [self addSubview:workTitleLoadLable];
    
    _workLoadLable = [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"" constraint:^(MASConstraintMaker *make) {
      make.top.mas_equalTo(mainView.mas_top).offset(20);
      make.left.mas_equalTo(mainView.left).offset(300);
      make.width.mas_equalTo(120);
      make.height.mas_equalTo(30);
    }];
    [self addSubview:_workLoadLable];
    
  }
  return self;
}

- (void)setCoursera:(TTCourseraModel *)coursera
{
  _coursera = coursera;
  [_iconImageView sd_setImageWithURL:[NSURL URLWithString:coursera.courseraIconLink]];
  _nameLable.text = coursera.courseraName;
  _workLoadLable.text = coursera.courseraWorkLoad;
  
}

@end
