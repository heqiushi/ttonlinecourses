//
//  TTCourseraDetileView.m
//  TTOnlineCourses
//
//  Created by viviya on 15/11/5.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTCourseraDetileView.h"
#import "UIView+quickLayoutInit.h"

@implementation TTCourseraDetileView

- (instancetype)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    __weak UIView *mainView = self;
    
    _iconImageView = [self addImageViewWithConstraint:^(MASConstraintMaker *make) {
      make.left.mas_equalTo(mainView.mas_left).offset(10);
      make.top.mas_equalTo(mainView.mas_top).offset(5);
      make.width.mas_equalTo(50);
      make.height.mas_equalTo(50);
    }];
    
    //name
    UILabel *nameTitleLable = [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"名称:" constraint:^(MASConstraintMaker *make) {
      make.left.mas_equalTo(mainView.mas_left).offset(65);
      make.top.mas_equalTo(mainView.mas_top).offset(15);
      make.width.mas_equalTo(50);
      make.height.mas_equalTo(30);
      
    }];
    
    _nameLable = [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"" constraint:^(MASConstraintMaker *make) {
      make.left.mas_equalTo(mainView.mas_left).offset(115);
      make.top.mas_equalTo(mainView.mas_top).offset(15);
      make.width.mas_equalTo(140);
      make.height.mas_equalTo(30);
      
    }];
    //languageLable
    
    [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"语言区域:" constraint:^(MASConstraintMaker *make) {
      make.left.mas_equalTo(mainView.mas_left).offset(250);
      make.top.mas_equalTo(mainView.mas_top).offset(15);
      make.width.mas_equalTo(100);
      make.height.mas_equalTo(30);
    }];
    
    _languageLable = [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"" constraint:^(MASConstraintMaker *make) {
      make.left.mas_equalTo(mainView.mas_left).offset(350);
      make.top.mas_equalTo(mainView.mas_top).offset(15);
      make.width.mas_equalTo(50);
      make.height.mas_equalTo(30);
      
    }];
    _languageLable.textAlignment = NSTextAlignmentLeft;

    //universityNameLable
    
    [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"大学名称:" constraint:^(MASConstraintMaker *make) {
      make.left.mas_equalTo(mainView.mas_left).offset(10);
      make.top.mas_equalTo(mainView.mas_top).offset(60);
      make.width.mas_equalTo(80);
      make.height.mas_equalTo(30);
    }];
    
    _universityNameLable = [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"" constraint:^(MASConstraintMaker *make) {
      make.left.mas_equalTo(mainView.mas_left).offset(100);
      make.top.mas_equalTo(mainView.mas_top).offset(60);
      make.width.mas_equalTo(150);
      make.height.mas_equalTo(30);
      
    }];
    
    
    //workLoadLable
    [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"工作量:" constraint:^(MASConstraintMaker *make) {
      make.left.mas_equalTo(mainView.mas_left).offset(10);
      make.top.mas_equalTo(mainView.mas_top).offset(100);
      make.width.mas_equalTo(80);
      make.height.mas_equalTo(30);
    }];
  
    _workLoadLable = [self addLabelWithFontSize:14 color:[UIColor blackColor] lines:1 text:@"" constraint:^(MASConstraintMaker *make) {
      make.left.mas_equalTo(mainView.mas_left).offset(70);
      make.top.mas_equalTo(mainView.mas_top).offset(100);
      make.width.mas_equalTo(150);
      make.height.mas_equalTo(30);
      
    }];
    
    _aboutCourseWebView = [[UIWebView alloc] init];
    [self addSubview:_aboutCourseWebView];
    [_aboutCourseWebView mas_makeConstraints:^(MASConstraintMaker *make) {
      make.top.mas_equalTo(self.mas_top).offset(140);
      make.left.mas_equalTo(self.mas_left).offset(10);
      make.right.mas_equalTo(self.mas_right).offset(-10);
      make.bottom.mas_equalTo(self.mas_bottom).offset(-10);
    }];
    
  }
  return self;
}

- (void)setCoursera:(TTCourseraModel *)coursera
{
  self.nameLable.text = coursera.courseraName;
  self.universityNameLable.text = @"北京大学";
  self.languageLable.text = coursera.courseraLanguage;
  self.workLoadLable.text = coursera.courseraWorkLoad;
  [self.aboutCourseWebView loadHTMLString:coursera.aboutTheCourse baseURL:nil];
  [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:coursera.courseraIconLink]];
}

@end
