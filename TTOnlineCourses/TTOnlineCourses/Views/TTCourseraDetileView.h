//
//  TTCourseraDetileView.h
//  TTOnlineCourses
//
//  Created by viviya on 15/11/5.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTBaseView.h"
#import "TTCourseraModel.h"

@interface TTCourseraDetileView : TTBaseView<UIWebViewDelegate>

@property(nonatomic,strong)UILabel        *nameLable;//名称
@property(nonatomic,strong)UIImageView    *iconImageView;//icon图标
@property(nonatomic,strong)UILabel        *universityNameLable;//大学名称
@property(nonatomic,strong)UILabel        *workLoadLable;//工作量
@property(nonatomic,strong)UILabel        *languageLable;//语言
@property(nonatomic,strong)UIWebView      *aboutCourseWebView;//描述

- (void)setCoursera:(TTCourseraModel *)coursera;
@end
