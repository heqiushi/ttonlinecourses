//
//  TTCourseraView.h
//  TTOnlineCourses
//
//  Created by viviya on 15/11/4.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTBaseView.h"

@interface TTCourseraView : TTBaseView
@property (nonatomic, strong) UICollectionView   *courseraCollectionVIew;//课程列表展示

@end
