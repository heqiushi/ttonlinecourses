//
//  KLUserProfileCollectionViewCell.h
//  Kunlun
//
//  Created by chuyanling on 15/7/4.
//  Copyright © 2015年 hui.wang. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "Macros.h"
#import "TTCourseraModel.h"

@interface TTCourseraCell : UICollectionViewCell

+ (CGFloat)cellHeight;

+ (NSString *)defaultIdentifier;


@property (nonatomic, strong) TTCourseraModel *coursera;


@end
