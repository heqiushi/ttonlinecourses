//
//  TTCourseraView.m
//  TTOnlineCourses
//
//  Created by viviya on 15/11/4.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTCourseraView.h"

@implementation TTCourseraView


- (instancetype)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    _courseraCollectionVIew = [self buildCollectionView];
    _courseraCollectionVIew.alwaysBounceVertical = NO;
    [self addSubview:_courseraCollectionVIew];
  }
  return self;
}



- (UICollectionView *)buildCollectionView
{
  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
  UICollectionView *collectionView =
  [[UICollectionView alloc] initWithFrame:CGRectZero
                     collectionViewLayout:flowLayout];
  collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
  collectionView.backgroundColor = [UIColor clearColor];
  return collectionView;
}

@end
