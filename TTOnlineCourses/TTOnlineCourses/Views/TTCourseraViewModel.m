//
//  TTCourseraViewModel.m
//  TTOnlineCourses
//
//  Created by viviya on 15/11/4.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTCourseraViewModel.h"

//static NSString *kCourseraInfoURL = @"includes=sessions";
static NSString *kCourseraURL = @"courses";//课程



@implementation TTCourseraViewModel

- (void)fetchCourseraInfo
{
  NSDictionary *param = @{@"fields": @"language,shortDescription,smallIcon,estimatedClassWorkload,universityLogoSt,language,aboutTheCourse"};
  [self sendGetWithPath:kCourseraURL params:param completeHandler:^(MKNetworkOperation *operation) {
    //处理逻辑
    //进行再次存储，用户可能已经修改资料
    DLog(@"FETCHUSERINFO RESULT ==== %@", operation.responseString);
    self.result = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
    if (self.result == nil || [self.result isKindOfClass:[NSNull class]]) {
      self.message = responseDataNullHint;
      return ;
    }
    self.status = TT_COURSERAVIEW_MODEL_REQUEST_RESULT_SUCCESS;
  } errorHandler:^(MKNetworkOperation *operation, NSError *error) {
    DLog(@"fetchUserInfo error =====%@", [error description]);
    self.message = [error description];
    self.status = TT_COURSERAVIEW_MODEL_REQUEST_RESULT_FAILED;
  }];
}
@end
