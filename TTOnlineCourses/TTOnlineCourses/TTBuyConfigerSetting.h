//
//  TTBuyConfigerSetting.h
//  SXBuy
//
//  Created by chuyanling on 15/4/5.
//  Copyright (c) 2015年 owner. All rights reserved.
//

#ifndef TTBuy_SXBuyConfigerSetting_h
#define TTBuy_SXBuyConfigerSetting_h

#import "UIColor+QuickInit.h"
#import "UIView+ShowTip.h"
#import "UIView+setFrame.h"
#import "UIImage+TintColor.h"


/**
 *  程序UI配置
 */
static NSString *const NAV_TINT_COLOR = @"#05A5A5";
static const CGFloat MBMessage_Duration = 0.8f;

#define ITEM_GAP   4
#define HIGHT_LIGHT_COLOR C_RGB(0xe10241)

#endif
