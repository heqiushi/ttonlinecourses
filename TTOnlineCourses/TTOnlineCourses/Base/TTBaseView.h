//
//  TTBaseView.h
//  TTOnlineCourses
//
//  Created by viviya on 15/11/3.
//  Copyright © 2015年 heqiushi. All rights reserved.
//
// Masonry
//define this constant if you want to use Masonry without the 'mas_' prefix
#define MAS_SHORTHAND

//define this constant if you want to enable auto-boxing for default syntax
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"

#import <UIKit/UIKit.h>
//视图基类
@interface TTBaseView : UIView
- (UIViewController*)viewController;
@end
