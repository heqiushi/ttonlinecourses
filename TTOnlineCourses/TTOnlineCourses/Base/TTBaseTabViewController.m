//
//  TCBaseTabViewController.m
//  Telecom
//
//  Created by Hyacinth on 14-7-6.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#import "TTBaseTabViewController.h"
#import "TTabItemModel.h"
#import "TTConfigHelper.h"

@interface TTBaseTabViewController ()
{
  NSArray *_tabbarItemModels;
}

@end

@implementation TTBaseTabViewController

- (instancetype)init
{
  self = [super init];
  if (self) {
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  [self.tabBar setBackgroundImage:[UIImage imageNamed:@"tabbarSelected_BG.jpg"]];
  [self.tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar_BG"]];
  _tabbarItemModels = [TTabItemModel tabItems];
  [self setupViewControllers];
  [self configUICompliance];
  self.selectedIndex = 0;
}


- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
  
  TTabItemModel *barItemModel = _tabbarItemModels[self.selectedIndex];
  self.navigationItem.title = barItemModel.title;
  
}

- (void)setupViewControllers
{
  NSMutableArray *viewControllers = [NSMutableArray new];
  
  
  for (TTabItemModel *barItemModel in _tabbarItemModels) {
    
    UIViewController *viewController = [NSClassFromString(barItemModel.vcName) new];
    viewController.title = barItemModel.title;
    UITabBarItem *barItem = [UITabBarItem new];
    [barItem setTitleTextAttributes:[TTConfigHelper tabItemNormalAttributes] forState:UIControlStateNormal];
    [barItem setTitleTextAttributes:[TTConfigHelper tabItemSelectedAttributes] forState:UIControlStateSelected];
    barItem.title = barItemModel.title;
    [barItem setFinishedSelectedImage:[UIImage imageNamed:barItemModel.selectedImageName] withFinishedUnselectedImage:[UIImage imageNamed:barItemModel.normalImageName]];
    viewController.tabBarItem = barItem;
    [viewControllers addObject:viewController];
  }
  self.viewControllers = viewControllers;
}



- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
  NSInteger selectedIndex = [tabBar.items indexOfObject:item];
  TTabItemModel *barItemModel = _tabbarItemModels[selectedIndex];
  self.navigationItem.title = barItemModel.title;
}


- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark ================= EVENTS ===================


- (void)configUICompliance
{
  if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
  }else{
    self.wantsFullScreenLayout = YES;
    
  }
}

- (BOOL)prefersStatusBarHidden
{
  return NO;
}


#ifdef __IPHONE_7_0

- (UIStatusBarStyle)preferredStatusBarStyle
{
  return UIStatusBarStyleLightContent;
}

#endif


- (NSUInteger)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
  return UIInterfaceOrientationPortrait;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
  [self.view endEditing:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
