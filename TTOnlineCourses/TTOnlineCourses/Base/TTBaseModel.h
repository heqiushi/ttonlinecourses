//
//  TTBaseModel.h
//  TTOnlineCourses
//
//  Created by viviya on 15/11/3.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import <Foundation/Foundation.h>
//数据模型基类


@protocol TTModelParseDelate;

@interface TTBaseModel : NSObject

+ (instancetype)modelWithData:(NSData *)data;
+ (instancetype)modelWithJson:(NSString *)json;

@end

@protocol TTBaseModelParseDelate <NSObject>

@optional
+ (instancetype)modelWithJson:(NSString *)json;
+ (NSArray *)modelsWithJson:(NSString *)json;
+ (instancetype)modelWithData:(NSData *)data;
+ (NSArray *)modelsWithData:(NSData *)data;
@end
