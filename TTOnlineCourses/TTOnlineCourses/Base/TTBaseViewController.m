//
//  TTBaseViewController.m
//  TTOnlineCourses
//
//  Created by viviya on 15/11/3.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTBaseViewController.h"
#import "UIViewController+MBProgressHud.h"

@interface TTBaseViewController ()

@end

@implementation TTBaseViewController

- (instancetype)init
{
  self = [super  init];
  if (self) {
    
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
}

#pragma mark ---- MBProgressHud 显示 -----------

- (void)shownLoadingHintText:(NSString *)text
{
  [self hideLoading];
  if ([text isEqualToString:@""] || text==nil) {
    [self showLoadingWithMask];
    return;
  }
  [self showLoadingWithHint:text];
}

- (void)shownErrorInfo:(NSString *)text
{
  [self hideLoading];
  [self showMessage:text duration:MBMessage_Duration];
}


#pragma  mark ------ configure ----------------

- (void)configUICompliance
{
  if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
  }else{
    self.wantsFullScreenLayout = YES;
  }
}





- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
  return NO;
}

#ifdef __IPHONE_7_0

- (UIStatusBarStyle)preferredStatusBarStyle
{
  return UIStatusBarStyleLightContent;
}

#endif


- (NSUInteger)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
  return UIInterfaceOrientationPortrait;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
  [self.view endEditing:YES];
}



@end
