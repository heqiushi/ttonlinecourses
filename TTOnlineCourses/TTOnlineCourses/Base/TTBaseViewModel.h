//
//  TTBaseViewModel.h
//  TTOnlineCourses
//
//  Created by viviya on 15/11/3.
//  Copyright © 2015年 heqiushi. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "TCReachabilityStatus.h"


//view model 基类，项目采用MVVM，引进此ViewModel基类
typedef NS_ENUM(NSUInteger, HTTP_METHOD_TYPE)
{
  HTTP_METHOD_TYPE_GET,
  HTTP_METHOD_TYPE_POST,
  HTTP_METHOD_TYPE_PULL,
  HTTP_METHOD_TYPE_PUT,
};

extern NSString *kResultKey;  //接口返回为数组时，KEY 值
extern NSString *kKVOKeyPath;  //status KVO path
extern NSString *const responseDataNullHint;  //无数据返回提示
extern NSString *const networkNoneHint;

typedef void(^completeHandler)(MKNetworkOperation *operation);
typedef void(^errorHandler)(MKNetworkOperation *operation, NSError *error);


@interface TTBaseViewModel : NSObject

@property (assign, nonatomic) NSUInteger status;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) id        result;


- (MKNetworkOperation *)sendRequestType:(HTTP_METHOD_TYPE)type path:(NSString *)path params:(NSDictionary *)params completeHandler:(completeHandler)completeHandler errorHandler:(errorHandler)errorHandler;

- (MKNetworkOperation *)sendSinaRequestType:(HTTP_METHOD_TYPE)type path:(NSString *)path params:(NSDictionary *)params completeHandler:(completeHandler)completeHandler errorHandler:(errorHandler)errorHandler;

- (MKNetworkOperation *)sendSinaGetWithPath:(NSString *)path params:(NSDictionary *)params completeHandler:(completeHandler)completeHandler errorHandler:(errorHandler)errorHandler;

- (MKNetworkOperation *)sendGetWithPath:(NSString *)path params:(NSDictionary *)params completeHandler:(completeHandler)completeHandler errorHandler:(errorHandler)errorHandler;

- (MKNetworkOperation *)sendPostWithPath:(NSString *)path params:(NSDictionary *)params completeHandler:(completeHandler)completeHandler errorHandler:(errorHandler)errorHandler;

- (BOOL)isReachable;

//缓存字典数据
- (void)archieveResultWithKeyUrlPath:(NSString *)key;
- (NSArray *)unArchieveResultWithKeyUrlPath:(NSString *)key;


@end
