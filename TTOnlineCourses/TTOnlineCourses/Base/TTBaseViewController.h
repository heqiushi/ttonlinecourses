//
//  TTBaseViewController.h
//  TTOnlineCourses
//
//  Created by viviya on 15/11/3.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import <UIKit/UIKit.h>
//vc基类
@interface TTBaseViewController : UIViewController
@property (nonatomic) BOOL needBackBtn;


- (void)shownLoadingHintText:(NSString *)text;
- (void)shownErrorInfo:(NSString *)text;

@end