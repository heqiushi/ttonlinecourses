//
//  TTBaseViewModel.m
//  TTOnlineCourses
//
//  Created by viviya on 15/11/3.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTBaseViewModel.h"
#import "TCNetworkEngine.h"

NSString *kResultKey = @"result";
NSString *kKVOKeyPath = @"status";
NSString *const responseDataNullHint = @"无数据返回";
NSString *const networkNoneHint = @"当前网络不稳定";

static inline NSString *HttpMethodDescription(HTTP_METHOD_TYPE type)
{
  NSString *presence = @"";
  switch (type) {
    case HTTP_METHOD_TYPE_GET:
      presence = @"GET";
      break;
    case HTTP_METHOD_TYPE_POST:
      presence = @"POST";
      break;
    case HTTP_METHOD_TYPE_PULL:
      presence = @"PULL";
      break;
    case HTTP_METHOD_TYPE_PUT:
      presence = @"PUT";
      break;
  }
  return presence;
  
}

@implementation TTBaseViewModel

- (MKNetworkOperation *)sendRequestType:(HTTP_METHOD_TYPE)type path:(NSString *)path params:(NSDictionary *)params completeHandler:(completeHandler)completeHandler errorHandler:(errorHandler)errorHandler
{
  DLog(@"HttpMethodDescription(type) == %@", HttpMethodDescription(type));
  TCNetworkEngine *engine = [TCNetworkEngine sharedEngine];
  MKNetworkOperation *operation = [engine operationWithPath:path params:params httpMethod:HttpMethodDescription(type)];
  [operation addCompletionHandler:completeHandler errorHandler:errorHandler];
  
  [engine enqueueOperation:operation];
  return operation;
}

- (MKNetworkOperation *)sendGetWithPath:(NSString *)path params:(NSDictionary *)params completeHandler:(completeHandler)completeHandler errorHandler:(errorHandler)errorHandler
{
  return [self sendRequestType:HTTP_METHOD_TYPE_GET path:path params:params completeHandler:completeHandler errorHandler:errorHandler];
}

- (MKNetworkOperation *)sendPostWithPath:(NSString *)path params:(NSDictionary *)params completeHandler:(completeHandler)completeHandler errorHandler:(errorHandler)errorHandler
{
  return [self sendRequestType:HTTP_METHOD_TYPE_POST path:path params:params completeHandler:completeHandler errorHandler:errorHandler];
}
//post data
- (MKNetworkOperation *)sendPostWithBodyString:(NSString *)string textType:(NSString *)textType path:(NSString *)path completeHandler:(completeHandler)completeHandler errorHandler:(errorHandler)errorHandler
{
  TCNetworkEngine *engine = [TCNetworkEngine sharedEngine];
  MKNetworkOperation *operation = [engine operationWithPath:path params:nil httpMethod:HttpMethodDescription(HTTP_METHOD_TYPE_POST)];
  [operation setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
    return string;
    DLog(@"Post Body String ==== %@", string);
  } forType:textType];
  [engine enqueueOperation:operation];
  return operation;
}


- (BOOL)isReachable
{
  return [TCNetworkEngine sharedEngine].isReachable;
}

- (void)archieveResultWithKeyUrlPath:(NSString *)key
{
  NSData *resultData = [NSKeyedArchiver archivedDataWithRootObject:self.result];
  [[NSUserDefaults standardUserDefaults] setObject:resultData forKey:key];
}

- (id)unArchieveResultWithKeyUrlPath:(NSString *)key
{
  NSData *cacheData = [[NSUserDefaults standardUserDefaults] objectForKey:key];
  if (cacheData == nil || [cacheData length]==0) {
    return nil;
  }
  id cacheResult =[NSKeyedUnarchiver unarchiveObjectWithData:cacheData];
  return cacheResult;
}

@end
