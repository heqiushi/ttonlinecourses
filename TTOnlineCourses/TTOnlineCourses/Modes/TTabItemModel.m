//
//  TCTabItemViewModel.m
//  Telecom
//
//  Created by Hyacinth on 14-7-6.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#import "TTabItemModel.h"
#import "Images.h"
#import "TTHomeViewController.h"
static NSString *kFrontPageVCName = @"TTHomeViewController";
static NSString *kActivityVCName = @"TTSearCourseraViewController";
static NSString *kPersonalCenterVCName = @"TTSearCourseraViewController";
static NSString *kMoreVCName = @"TTSearCourseraViewController";

static NSString *kFrontPageTitle = @"首页";
static NSString *kActivityTitle = @"活动";
static NSString *kPersonalCenterTitle = @"个人信息";
static NSString *kMoreTitle = @"更多";

@implementation TTabItemModel

+ (NSArray *)tabItems
{
	NSMutableArray *tabItems = [NSMutableArray new];
	
	TTabItemModel *front = [TTabItemModel new];
	front.vcName = NSStringFromClass([TTHomeViewController class]);
	front.title = kFrontPageTitle;
	front.normalImageName = kFrontPageTabbarItemNormalImage;
	front.selectedImageName = kFrontPageTabbarItemSelectedImage;
	
	[tabItems addObject:front];
	
	TTabItemModel *activity = [TTabItemModel new];
	activity.vcName = kActivityVCName;
	activity.title = kActivityTitle;
	activity.normalImageName = kActivityTabbarItemNormalImage;
	activity.selectedImageName = kActivityTabbarItemSelectedImage;
	
	[tabItems addObject:activity];
	
	TTabItemModel *personalCenter = [TTabItemModel new];
	personalCenter.vcName = kPersonalCenterVCName;
	personalCenter.title = kPersonalCenterTitle;
	personalCenter.normalImageName = kPersonalCenterTabbarItemNormalImage;
	personalCenter.selectedImageName = kPersonalCenterTabbarItemSelectedImage;
	
	[tabItems addObject:personalCenter];
	
	TTabItemModel *more = [TTabItemModel new];
	more.vcName = kMoreVCName;
	more.title = kMoreTitle;
	more.normalImageName = kMoreTabbarItemNormalImage;
	more.selectedImageName = kMoreTabbarItemSelectedImage;
	
	[tabItems addObject:more];
	
	return tabItems;
}
@end
