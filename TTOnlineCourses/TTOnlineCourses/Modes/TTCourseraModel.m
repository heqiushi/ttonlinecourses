//
//  TTCourseraModel.m
//  TTOnlineCourses
//
//  Created by viviya on 15/11/4.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTCourseraModel.h"

@interface TTCourseraModel ()
@property (copy, nonatomic) NSString *courseraID; //活动ID
@property (copy, nonatomic) NSString *courseraName;  //活动名称
@property (copy, nonatomic) NSString *courseraIconLink;  //课程的图片icon连接
@property (copy, nonatomic) NSString *courseraWorkLoad; //工作量
@property (copy, nonatomic) NSString *courseraLanguage; //课程语言
@property (copy, nonatomic) NSString *aboutTheCourse; //课程描述

@end


@implementation TTCourseraModel

+ (instancetype)modelWithDict:(NSDictionary *)dict
{
  return [[self alloc] initWithDict:dict];
}


- (instancetype)initWithDict:(NSDictionary *)dict
{
  self =[super init];
  if (self) {
    _courseraID = check_parm_is_NSNull_NO([dict objectForKey:@"id"]);
    _courseraName = check_parm_is_NSNull_NO([dict objectForKey:@"name"]);
    _courseraIconLink = check_parm_is_NSNull_NO([dict objectForKey:@"smallIcon"]);
    _courseraWorkLoad = check_parm_is_NSNull_NO([dict objectForKey:@"estimatedClassWorkload"]);
    _courseraLanguage = check_parm_is_NSNull_NO([dict objectForKey:@"language"]);
    _aboutTheCourse = check_parm_is_NSNull_NO([dict objectForKey:@"aboutTheCourse"]);

    
  }
  return self;
}

@end
