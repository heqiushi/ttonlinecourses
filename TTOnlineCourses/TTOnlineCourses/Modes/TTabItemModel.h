//
//  TCTabItemViewModel.h
//  Telecom
//
//  Created by Hyacinth on 14-7-6.
//  Copyright (c) 2014年 Hyacinth. All rights reserved.
//

#import "TTBaseModel.h"

@interface TTabItemModel : TTBaseModel

@property (strong, nonatomic) NSString *vcName;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *normalImageName;
@property (strong, nonatomic) NSString *selectedImageName;

+ (NSArray *)tabItems;
@end
