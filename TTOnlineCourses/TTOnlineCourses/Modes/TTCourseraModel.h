//
//  TTCourseraModel.h
//  TTOnlineCourses
//
//  Created by viviya on 15/11/4.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTBaseModel.h"

@interface TTCourseraModel : TTBaseModel

@property (copy, readonly, nonatomic) NSString *courseraID; //活动ID
@property (copy, readonly, nonatomic) NSString *courseraName;  //活动名称
@property (copy, readonly, nonatomic) NSString *courseraIconLink;  //课程的图片icon连接
@property (copy, readonly, nonatomic) NSString *courseraWorkLoad; //工作量
@property (copy, readonly, nonatomic) NSString *courseraLanguage; //课程语言
@property (copy, readonly, nonatomic) NSString *aboutTheCourse; //课程描述

+ (instancetype)modelWithDict:(NSDictionary *)dict;

@end
