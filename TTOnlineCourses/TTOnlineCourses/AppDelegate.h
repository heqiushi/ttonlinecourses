//
//  AppDelegate.h
//  TTOnlineCourses
//
//  Created by viviya on 15/11/3.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

@end

