//
//  TTCourseraDetileViewController.m
//  TTOnlineCourses
//
//  Created by viviya on 15/11/5.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTCourseraDetileViewController.h"
#import "TTCourseraDetileView.h"

@interface TTCourseraDetileViewController ()


@property(nonatomic,strong)TTCourseraDetileView *courseraDetileView;
@end

@implementation TTCourseraDetileViewController

- (id)init
{
  self = [super init];
  if (self) {
    //    self.needBackBtn = YES;
    _courseraDetileView = [[TTCourseraDetileView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
  }
  return self;
}

- (void)loadView
{
  self.view = _courseraDetileView;
}


- (void)viewDidLoad {
  [super viewDidLoad];
  [self.courseraDetileView setCoursera:self.courseraModel];
}




@end
