//
//  TTHomeViewController.m
//  TTOnlineCourses
//
//  Created by viviya on 15/11/3.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTHomeViewController.h"
#import "TTCourseraViewModel.h"
#import "TTCourseraModel.h"
#import "TTCourseraCell.h"
#import "TTCourseraView.h"
#import "TTCourseraDetileViewController.h"
#import "SDRefresh.h"

@interface TTHomeViewController ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) TTCourseraViewModel *courseraViewModel; //活动ViewModel
@property (strong, nonatomic) TTCourseraView      *courseraView;//视图view
@property (strong, nonatomic) NSMutableArray      *courseraListMutableArray;//课程列表
@property (nonatomic, weak) SDRefreshFooterView   *refreshFooter;
@property (nonatomic, assign) NSInteger totalRowCount;


@end

@implementation TTHomeViewController


- (id)init
{
  self = [super init];
  if (self) {
    self.needBackBtn = YES;
    self.totalRowCount = 0;
    _courseraViewModel = [[TTCourseraViewModel alloc] init];
    _courseraListMutableArray = [NSMutableArray array];
    //猜你喜欢数据列表变化KVO监听注册
    [_courseraViewModel addObserver:self
                         forKeyPath:@"status"
                            options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew
                            context:(__bridge void*)self];
    self.courseraView = [[TTCourseraView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
  }
  return self;
}

- (void)loadView
{
  _courseraView.courseraCollectionVIew.delegate=self;
  _courseraView.courseraCollectionVIew.dataSource = self;
  [_courseraView.courseraCollectionVIew registerClass:[TTCourseraCell class]
                 forCellWithReuseIdentifier:[TTCourseraCell defaultIdentifier]];
  _courseraView.courseraCollectionVIew.backgroundColor = [UIColor clearColor];
  [_courseraView.courseraCollectionVIew makeConstraints:^(MASConstraintMaker *make) {
    make.left.mas_equalTo(_courseraView.mas_left).offset(0);
    make.top.mas_equalTo(_courseraView.mas_top).offset(0);
    make.right.mas_equalTo(_courseraView.mas_right).offset(0);
    make.bottom.mas_equalTo(_courseraView.mas_bottom).offset(0);
  }];
  self.view = _courseraView;
  
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  [_courseraViewModel fetchCourseraInfo];
  [self setupFooter];
}

- (void)setupFooter
{
  // 进入页面自动加载一次数据
  
  SDRefreshFooterView *refreshFooter = [SDRefreshFooterView refreshView];
  [refreshFooter addToScrollView:self.courseraView.courseraCollectionVIew];
  [refreshFooter addTarget:self refreshAction:@selector(footerRefresh)];
  _refreshFooter = refreshFooter;

}

- (void)footerRefresh
{
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    if (self.totalRowCount>=[_courseraListMutableArray count]) {
      [self showMessage:@"已经是最后一条了" duration:.2];
      [self.refreshFooter endRefreshing];
      [self.refreshFooter setHidden:YES];
    }else
    {
      self.totalRowCount += 10;
      if (self.totalRowCount>=[_courseraListMutableArray count]) {
        self.totalRowCount= [_courseraListMutableArray count];
      }
      [self.courseraView.courseraCollectionVIew reloadData];
      [self.refreshFooter endRefreshing];
    }
    
  });
}

#pragma mark KVO 监听
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
  
  if ((__bridge id)context == self) {
    if ([keyPath isEqualToString:@"status"]) {
      switch ([[change objectForKey:NSKeyValueChangeNewKey]integerValue]) {
          //监听猜你喜欢处理
        case TT_COURSERAVIEW_MODEL_REQUEST_RESULT_SUCCESS:
        {
          for (NSDictionary *dic in [(NSDictionary*)self.courseraViewModel.result objectForKey:@"elements"]) {
            TTCourseraModel *activityModel = [TTCourseraModel modelWithDict:dic];
            [_courseraListMutableArray addObject:activityModel];
            //            if ([_courseraListMutableArray count]>20) {
            //              break;
            //            }
          }
          self.totalRowCount = 10;
          [_courseraView.courseraCollectionVIew reloadData];
        }
          break;
        case TT_COURSERAVIEW_MODEL_REQUEST_RESULT_FAILED:
        {
          [self showMessage:@"获取课程列表失败" duration:.2];
        }
          break;
        default:
          break;
      }
    }
    
  } else {
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
  }
}

#pragma mark - UICollectionView Delegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
  return 0.f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
  return 5.f;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
  return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  return CGSizeMake(CGRectGetWidth(collectionView.frame), [TTCourseraCell cellHeight]);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  return self.totalRowCount;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  TTCourseraCell *cell =
  [collectionView dequeueReusableCellWithReuseIdentifier:[TTCourseraCell defaultIdentifier]
                                            forIndexPath:indexPath];
  [cell setCoursera:[_courseraListMutableArray objectAtIndex:indexPath.row]];
  //TODO: 界面赋值
  return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  TTCourseraDetileViewController *courseraDetileViewController =
   [[TTCourseraDetileViewController alloc] init];
  courseraDetileViewController.courseraModel = [_courseraListMutableArray objectAtIndex:indexPath.row];
  self.navigationController.navigationBar.translucent = NO;

  [self.navigationController pushViewController:courseraDetileViewController animated:YES];
}
- (void)dealloc{
  //kvo 监听销毁
  [_courseraViewModel removeObserver:self forKeyPath:@"status"];
}

@end
