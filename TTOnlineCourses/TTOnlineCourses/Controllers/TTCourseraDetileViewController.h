//
//  TTCourseraDetileViewController.h
//  TTOnlineCourses
//
//  Created by viviya on 15/11/5.
//  Copyright © 2015年 heqiushi. All rights reserved.
//

#import "TTBaseViewController.h"
#import "TTCourseraModel.h"

@interface TTCourseraDetileViewController : TTBaseViewController

@property(nonatomic,strong)TTCourseraModel    *courseraModel;
@end
